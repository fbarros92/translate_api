# README #

### What is this repository for? ###

An Flask API witch receives three params (input_word, input_language, output_language).

Once given the parameters the api returns the translated word/sentence accordly to the parameter 'output_language' (English, Spanish and Portuguese) .

Currently, this API only give back those three languanges, can be extended due necessity.

The API is available to be used at: http://servicetranslation-env.eba-qvsfinuy.us-east-1.elasticbeanstalk.com/translate
