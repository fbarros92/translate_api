import json
import requests as r
import time
import pandas as pd
import urllib
from flask import Flask, request

def get_query(drv, query):
    data = r.get('http://wow_apps.mejoracontinua-merqueo.com/get_query_plumber',
                  params={'query': urllib.parse.quote(query),
                          'server': drv})
    result = pd.read_json(data.content)
    return result

def send_query(data, table_name):
        guardado = False
        while guardado == False:
            data_json = data.to_json(orient='records')
            data2 = r.get('http://wow_apps.mejoracontinua-merqueo.com/send_query_plumber',
                           data={'json': data_json,
                                 'table_name': table_name}, timeout=(0.5, 1))
            print(data2.status_code)
            if data2.status_code == 200:
                print(data_json)
                guardado = True
            else:
                print("no guardo")
                time.sleep(1)

application = Flask(__name__)
@application.route('/translate')
def translate():
    '''Returns the translated word, given the required parameters:
    * input: the desired word to be translated
    * input_lang: using the three achronym shown below, to inform the input language
    * output_lang: using the three achronym shown below, select the translation language
    
    Accepted languages
    - Spanish    -> spa
    - English    -> eng
    - Portuguese -> por
    '''
    request_data = request.get_json()
    input = request_data['input']
    input_lang = request_data['input_lang']
    output_lang = request_data['output_lang']

    # Check if it's a sentence exists into the database, if don't, add to the database
    temp_input = input.replace(' ','+')
    try:
        # Retrieve the word/sentence with the desired language
        db_return = get_query('shiny',f'select {output_lang} from languages where {input_lang}="{temp_input}"')[:][f'{output_lang}'][0]
        return db_return.replace('+',' ')
    except KeyError:
        print("That's a new word!")
        dict_langs = {'spa':[], 'eng':[], 'por':[]}
          
        dict_langs[input_lang].append(input)   
        for k,v in dict_langs.items():
            if not len(v):
                # Automatically looks for the translation on the internet
                s = r.Session()
                payload = {
                    'format':'text',
                    'from':input_lang,
                    'input':input,
                    'options':{
                        'contexResults':True,
                        'languageDetection': True,
                        'origin':'translation.web',
                        'sentenceSplitter':True
                    },
                    'to':k}
                headers = {
                    'Accept': 'application/json, text/plain, */*',
                    'Accept-Encoding':'gzip, deflate, br',
                    'Accept-Language': 'pt-PT,pt;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Connection': 'keep-alive',
                    'Content-Length': '170',
                    'Content-Type': 'application/json',
                    'Host': 'api.reverso.net',
                    'Origin': 'https://www.reverso.net',
                    'Referer': 'https://www.reverso.net/',
                    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
                    'sec-ch-ua-mobile': '?0',
                    'sec-ch-ua-platform': '"Windows"',
                    'Sec-Fetch-Dest': 'empty',
                    'Sec-Fetch-Mode': 'cors',
                    'Sec-Fetch-Site': 'same-site',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36',
                    'X-Reverso-Origin': 'translation.web'}
                try:
                    res = s.post('https://api.reverso.net/translate/v1/translation', json=payload, headers=headers) 
                    while not res.ok:
                        print('Error at the translation. . . Trying again. . .')
                        res = s.post('https://api.reverso.net/translate/v1/translation', json=payload, headers=headers) 
                    translated_word = json.loads(res.text)['translation'][0]
                    dict_langs[k].append(str(translated_word))
                except:
                    return 'Error during the translation'
        print(dict_langs)
        
        data = pd.DataFrame(dict_langs)
        send_query(data=data,table_name='languages')
        print('SAVED!!!') 
        
        # Retrieve the word/sentence with the desired language 
        db_return = get_query('shiny',f'select {output_lang} from languages where {input_lang}="{temp_input}"')[:][f'{output_lang}'][0]
        return db_return.replace('+',' ')

if __name__ == '__main__':
    application.run(debug=True, port=80, host = "0.0.0.0")
    # application.run()

